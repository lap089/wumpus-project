

from wumpus_map import *
from wumpus_agent import *
import MeowLogger
from AppGui import *

def main():
    MeowLogger.reset_log()

    map = MapReader("wumpus_map.txt")

    mapsize = map.height

    #
    # forward_program = AlwaysForwardProgram()
    #

    explorer_agent = HybridWumpusAgent()

    wumpus_env = WumpusEnvironment(explorer_agent=explorer_agent, width=map.width+2, height=map.height+2, map_reader=map) #init the wumpus environment

    #
    #
    # # gui
    root = Tk()
    app = AppGui(root, wumpus_env)
    root.wm_title("Final Project")
    root.mainloop()



if __name__ == "__main__":
    main()