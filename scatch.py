from logic import *

t = 5

have_arrow = Expr("HaveArrow{}".format(t))
wumpus_alive = Expr("WumpusAlive{}".format(t))
ret = have_arrow & wumpus_alive

print(ret)

"HaveArrow{}".format(t) + ' & ' + "WumpusAlive{}".format(t)