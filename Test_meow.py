from logic import *
import re
from wumpus_agent import  *
import MeowLogger

def add_axioms_to_kb(axioms, kb):
    for sentence in axioms.splitlines():
        if len(re.findall(r"[\w]+",sentence)) != 0:
            kb.tell(expr(sentence))
            if(minisat(kb.clauses)) == False:
                return False
    return True

kb = PropKB_SAT()
a = """~(W1_1|P1_1)&L1_1_0
 B1_1% (P2_1|P1_2)

 B2_1% (P1_1|P2_2)

 S1_1% (W2_1|W1_2)

 S2_1% (W1_1|W2_2)

 W1_1 | W1_2 | W2_1 | W2_2
 (~W1_1 | ~W1_2)&(~W1_1 | ~W2_1)&(~W1_1 | ~W2_2)&(~W1_2 | ~W2_1)&(~W1_2 | ~W2_2)&(~W2_1 | ~W2_2)
 L1_1_0 & ~(L1_2_0|L2_1_0|L2_2_0)
 HeadingEast0 & ~(HeadingNorth0 | HeadingSouth0 | HeadingWest0)
 HaveArrow0 & WumpusAlive0
 ~Stench0 & ~Breeze0 & ~Glitter0 & ~Bump0 & ~Scream0

 (~P2_1 & (~W2_1 | (W2_1 & ~WumpusAlive0))) >> OK2_1_0

 L1_1_0 >> (Breeze0 % B1_1)

 L1_1_0 >> (Stench0 % S1_1)

 L2_1_0 >> (Stench0 % S2_1)

 ((L1_1_0 & HeadingEast0 & Forward0) >> (L2_1_1 & ~L1_1_1)) & ((L1_1_0 & HeadingNorth0 & Forward0) >> (L1_2_1 & ~L1_1_1)) & (L1_1_1 << ((Wait0 | Grab0 | Shoot0 | TurnLeft0 | TurnRight0) & L1_1_0))
 ((L2_1_0 & HeadingWest0 & Forward0) >> (L1_1_1 & ~L2_1_1)) & ((L2_1_0 & HeadingNorth0 & Forward0) >> (L2_2_1 & ~L2_1_1)) & (L2_1_1 << ((Wait0 | Grab0 | Shoot0 | TurnLeft0 | TurnRight0) & L2_1_0))



 (Forward0|Grab0|Shoot0|Climb0|TurnLeft0|TurnRight0|Wait0)&((~Forward0|~Grab0) & (~Forward0|~Shoot0) & (~Forward0|~Climb0) & (~Forward0|~TurnLeft0) & (~Forward0|~TurnRight0) & (~Forward0|~Wait0) & (~Grab0|~Shoot0) & (~Grab0|~Climb0) & (~Grab0|~TurnLeft0) & (~Grab0|~TurnRight0) & (~Grab0|~Wait0) & (~Shoot0|~Climb0) & (~Shoot0|~TurnLeft0) & (~Shoot0|~TurnRight0) & (~Shoot0|~Wait0) & (~Climb0|~TurnLeft0) & (~Climb0|~TurnRight0) & (~Climb0|~Wait0) & (~TurnLeft0|~TurnRight0) & (~TurnLeft0|~Wait0) & (~TurnRight0|~Wait0))
 Forward0


 ~Stench1 & ~Breeze1 & ~Glitter1 & ~Bump1 & ~Scream1


 (~P1_1 & (~W1_1 | (W1_1 & ~WumpusAlive1))) % OK1_1_1

 (~P2_1 & (~W2_1 | (W2_1 & ~WumpusAlive1))) % OK2_1_1


 L1_1_1 >> (Breeze1 % B1_1)
 L2_1_1 >> (Breeze1 % B2_1)
 L1_1_1 >> (Stench1 % S1_1)
 L2_1_1 >> (Stench1 % S2_1)
"""

alines = a.splitlines()
for l in alines:
    add_axioms_to_kb(l,kb)
    MeowLogger.log_to_file("TELL",l)
    test = minisat(kb.clauses)
    if test.success == False:
        MeowLogger.log_to_file("SatSol","FALSE")

#
# add_axioms_to_kb(a,kb)
# test = minisat(kb.clauses)
# print(test)
# query = expr("P1_1")
# result = kb.ask(query)
# print(result)