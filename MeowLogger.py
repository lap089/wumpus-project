filename = "MeowLog_Knowledgebase_tell_ask.txt"
action_log_name = "MeowLog_action.txt"

def reset_log():
    file = open(filename, 'a+')
    file.write("#_____________New Log start here____________________# \n")
    f2 = open(action_log_name, "a+")
    f2.write("_____________New Log start here____________________#\n")

def log_to_file(TAG ,m_log, res = None):
    m_log = str(m_log)
    file = open(filename, 'a+')
    file.write(TAG+" : ")
    file.write(m_log)
    if TAG == "ASK":
        if res == True:
            file.write(" True")
        else:
            file.write(" False")

    file.write("\n")
    file.close()



def log_action(act):
    file = open(action_log_name, 'a+')
    file.write("Action: "+act+"\n")
    file.close()

def position_log(position, direction):
    file = open(action_log_name, 'a+')
    file.write( str(position)+" "+str(direction)+"\n")
    file.close()